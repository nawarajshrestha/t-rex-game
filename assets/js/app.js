const game = document.getElementById("game");
const dino = document.getElementById("dino");

let isJumping = false;
let isGameOver = false;
let dinoPosition = 0;
let score = 0;


function keyHandler(e) {
  if (e.keyCode === 32) {
    // console.log("pressed");
    if (!isJumping) {
      jump();
    }
  }
}

document.addEventListener("keyup", keyHandler);

//setTimeout allows us to run a function once after the interval of time.
//setInterval allows us to run a function repeatedly, starting after the interval of time, then repeating continuously at that interval.

//jumping
function jump() {
  isJumping = true;

  //for up
  let upInterval = setInterval(() => {
    //moving up to 170px
    if (dinoPosition > 170) {
      //then stop in 170px
      clearInterval(upInterval);
      //for down
      let downInterval = setInterval(() => {
        // moving down to  bottom  0
        if (dinoPosition < 0) {
          //then stop at bottom 0
          clearInterval(downInterval);
          isJumping = false;
        } else {
          dinoPosition = dinoPosition - 10;
          dino.style.bottom = dinoPosition + "px";
        }
      }, 20);
    } else {
      dinoPosition = dinoPosition + 20;
      dino.style.bottom = dinoPosition + "px";
    }
  }, 20);
}

//obstacle

function createObstacle() {
  let randomTime = Math.random() * 5000;
  const obstacle = document.createElement("div");
  obstacle.classList.add("obstacle");
  game.appendChild(obstacle);
  let obstaclePosition = 1000;
  obstacle.style.left = obstaclePosition + "px";

  // for moving obstacles to left
  let timer = setInterval(() => {
    if (obstaclePosition > 0 && obstaclePosition < 50 && dinoPosition < 10) {
      clearInterval(timer);
      // alert("game over")
      game.innerHTML = `<div id="dino"></div>`;
      isGameOver = true;
      document.body.innerHTML = `<div class="restart">
            <span class="game-over" >Game Over</span>
            <Button  class="restart-button" onclick= location.reload()>Restart</Button>
            </div>`;
    }
    obstaclePosition = obstaclePosition - 10;
    obstacle.style.left = obstaclePosition + "px";
  }, 20);

  setTimeout(createObstacle, randomTime);
}
createObstacle();

//score
function scoreBoard() {
  let scoreTime = setInterval(() => {
    score++;
    let scoring = document.getElementById("scoring");
    scoring.innerHTML = "Your Score:" + score;
  }, 1000);
  console.log(scoreTime);
}

window.addEventListener("load", () => {
  console.log("page is fully loaded");
  scoreBoard();
});
